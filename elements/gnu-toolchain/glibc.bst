kind: manual

sources:
- kind: git
  url: upstream:glibc
  track: release/2.25/master
  ref: db0242e3023436757bbc7c488a779e6e3343db04

depends:
- filename: gnu-toolchain/stage2.bst
  type: build
- gnu-toolchain/fhs-dirs.bst
- gnu-toolchain/linux-api-headers.bst

variables:
  glibc-arch-flags: ''

  (?):
    - arch.startswith("armv7"):
        glibc-arch-flags: --without-fp

    - arch in ["armv8b64", "armv8l64", "ppc64b", "ppc64l", "x86_64"]:
        # Setting this will affect BuildStream's default %{libdir} which
        # affects the built-in split rules.
        lib: "lib64"

environment:
  PATH: /usr/bin:/bin:/usr/sbin:/sbin:/tools/bin:/tools/sbin

config:

  configure-commands:
  - mkdir o

  - |
    export CFLAGS="-O2 $CFLAGS";
    cd o && ../configure %{glibc-arch-flags} \
      --prefix="%{prefix}" \
      --disable-profile \
      --enable-kernel=3.0.0 \
      --without-cvs \
      --without-selinux \
      --enable-obsolete-rpc

  build-commands:
  - cd o && make localtime=UTC

  install-commands:
  - cd o && make install_root="%{install-root}" localtime=UTC install
  - cd o && make install_root="%{install-root}" localtime=UTC localedata/install-locales
  - mkdir -p "%{install-root}/etc"
  - mkdir -p "%{install-root}/etc/ld.so.conf.d"
  - |
    cat <<EOF > nsswitch.conf
    passwd: compat
    group: compat
    shadow: compat

    hosts: files myhostname mdns4_minimal [NOTFOUND=return] dns mdns4
    networks: files

    protocols: db files
    services: db files
    ethers: db files
    rpc: db files

    netgroup: nis
    EOF
  - install -m 644 -o root -g root nsswitch.conf "%{install-root}/etc/nsswitch.conf"
  - |
    cat <<EOF > ld.so.conf
    /lib
    /usr/lib
    /usr/local/lib
    include /etc/ld.so.conf.d/*.conf
    EOF
  - install -m 644 -o root -g root ld.so.conf "%{install-root}/etc/ld.so.conf"

  strip-commands:
  - |
    %{stage3-strip-binaries}

public:
  bst:
    split-rules:
      runtime:
        (>):
        - |
          %{sysconfdir}/*
        - |
          /%{lib}/ld*.so*
        - |
          /%{lib}/lib*.so*
        - |
          %{libdir}/*crt*.o

      devel:
        (>):
        - |
          /%{lib}/lib*.a
        - |
          /%{lib}/lib*.la

      locale:
        (>):
        - |
          %{libdir}/gconv
        - |
          %{libdir}/gconv/*
        - |
          %{prefix}/lib/locale
        - |
          %{prefix}/lib/locale/locale-archive
