kind: manual

description:
  Simple native compiler which is used to build all of stage 3.

sources:
- kind: git
  url: upstream:gcc-tarball
  track: baserock/gcc-7.1.0
  ref: 3b0dbcfa2e5d12bd80cab1b35f08653d61fe7838

depends:
- filename: gnu-toolchain/stage1.bst
  type: build
- gnu-toolchain/stage2-linux-api-headers.bst
- gnu-toolchain/stage2-glibc.bst
- gnu-toolchain/stage2-gcc-fixed-headers.bst
- gnu-toolchain/stage2-libstdcxx.bst

variables:
  prefix: /tools
  gcc-arch-flags: ''
  gcc-libdir: lib

  (?):
    # ARM platforms vary a lot even within a single architecture revision,
    # and require tweaks such as this to produce a useful compiler.
    # This is a default configuration that has worked for some targets.
    - arch in ["armv7b", "armv7l"]:
        gcc-arch-flags: --with-arch=armv7-a
    - arch in ["armv7blhf", "armv7lhf"]:
        gcc-arch-flags: >
                        --with-arch=armv7-a
                        --with-cpu=cortex-a9
                        --with-tune=cortex-a9
                        --with-fpu=vfpv3-d16
                        --with-float=hard

    # GCC is hardwired to support 32-bit and 64-bit in parallel on these
    # platforms, so 64-bit libraries end up in `lib64` rather than `lib`.
    - arch in ["armv8b64", "armv8l64", "x86_64"]:
        gcc-libdir: lib64

environment:
  PATH: /tools/bin:/usr/bin:/bin:/usr/sbin:/sbin

config:
  configure-commands:
  - mkdir o

  # In other projects we specify the sysroot location using CPPFLAGS.
  # Here, that breaks because GCC compiles stuff for the *build* machine,
  # too ...  and this requires using the host's compiler, which cannot use
  # the same set of CPPFLAGS as the target. If we specify the sysroot
  # using CC instead then we don't interfere, because we are only
  # specifying the *host* C compiler.
  #
  # Configure flag notes:
  #  1. It's vital that this compiler runs in the bootstrap machine, and
  #     targets the same machine (TARGET_STAGE1) so that the stage 1 GCC
  #     is used instead of the compiler of the build machine.
  #  2. See gcc.morph.
  #  3. Disable searching /usr/local/include for headers
  #  4. This flag causes the correct --sysroot flag to be passed when
  #     calling stage 1 GCC.
  - |
    export STAGE2_SYSROOT="$(dirname $(dirname $(pwd)))"
    export CC="%{target-stage1}-gcc --sysroot=$STAGE2_SYSROOT"
    export CXX="%{target-stage1}-g++ --sysroot=$STAGE2_SYSROOT"
    export AR="%{target-stage1}-ar"
    export RANLIB="%{target-stage1}-ranlib"
    cd o && ../configure %{gcc-arch-flags}                       \
              --build=$(sh ../config.guess)                      \
      `# [1]` --host=%{target-stage1}                            \
      `# [1]` --target=%{target-stage1}                          \
              --prefix="%{prefix}"                               \
      `# [2]` --libdir=%{prefix}/lib                             \
      `# [3]` --with-local-prefix=%{prefix}                      \
      `# [4]` --with-build-sysroot="$STAGE2_SYSROOT"             \
              --disable-bootstrap                                \
              --disable-nls                                      \
              --disable-multilib                                 \
              --disable-libgomp                                  \
              --disable-libstdcxx-pch                            \
              --enable-languages=c,c++

  build-commands:
  - |
    export STAGE2_SYSROOT="$(dirname $(dirname $(pwd)))"
    cd o && make

  install-commands:
  - cd o && make DESTDIR="%{install-root}" install

  # Stage 3 builds need to link against this file in the location that
  # it will be in the final system, so we make a temporary link now.
  - |
    install -d "%{install-root}/usr/lib"
    ln -s "%{prefix}/%{gcc-libdir}/libgcc_s.so" "%{install-root}/usr/lib/"
    ln -s "%{prefix}/%{gcc-libdir}/libgcc_s.so.1" "%{install-root}/usr/lib/"

  strip-commands:
  - |
    %{stage2-strip-binaries}

  (?):
    # GCC is not passing the correct host/target flags to GMP's configure
    # script, which causes it to not use the machine-dependent code for
    # the platform and use the generic one instead.  However, the generic
    # code results on an undefined reference to `__gmpn_invert_limb' in
    # ARMv7.  Fix the invocation of GMP's configure script so that GMP can
    # use the machine-dependent code.
    - arch.startswith("armv5"):
        build-commands:
          (<):
          - sed -i "s/--host=none/--host=armv5/" o/Makefile
          - sed -i "s/--target=none/--target=armv5/" o/Makefile
    - arch.startswith("armv7"):
        build-commands:
          (<):
          - sed -i "s/--host=none/--host=armv7a/" o/Makefile
          - sed -i "s/--target=none/--target=armv7a/" o/Makefile
