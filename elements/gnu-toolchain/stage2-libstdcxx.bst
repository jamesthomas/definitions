kind: manual

sources:
- kind: git
  url: upstream:gcc-tarball
  track: baserock/gcc-7.1.0
  ref: 3b0dbcfa2e5d12bd80cab1b35f08653d61fe7838

depends:
- filename: gnu-toolchain/stage1.bst
  type: build
- gnu-toolchain/stage2-linux-api-headers.bst
- gnu-toolchain/stage2-glibc.bst

variables:
  prefix: /tools

environment:
  PATH: /tools/bin:/usr/bin:/bin:/usr/sbin:/sbin

config:
  configure-commands:
  - mkdir o

  # Configure flag notes:
  #  1. The thread C++ library cannot be built, as the thread C library
  #     was not build in stage1-gcc.
  #  2. Prevents the installation of precompiled include files, which are
  #     not needed at this stage.
  #  3. From LFS: the header location of C++ needs to be explicitly given
  #     as we are running the configure script from the top-level
  #     directory.
  - |
    export STAGE2_SYSROOT="$(dirname $(dirname $(pwd)))"
    # -fPIC must be given, otherwise it will not be possible to create
    # shared libraries linked to libstdc++
    export CPPFLAGS="--sysroot=$STAGE2_SYSROOT -fPIC"
    export LDFLAGS="--sysroot=$STAGE2_SYSROOT"
    cd o && ../libstdc++-v3/configure                           \
              --build=$(sh ../config.guess)                     \
              --host="%{target-stage1}"                         \
              --target="%{target-stage1}"                       \
              --prefix="%{prefix}"                              \
              --disable-nls                                     \
              --disable-shared                                  \
              --disable-multilib                                \
      `# [1]` --disable-libstdcxx-threads                       \
      `# [2]` --disable-libstdcxx-pch                           \
      `# [3]` --with-gxx-include-dir=/tools/%{target-stage1}/include/c++/7.1.0

  build-commands:
  - cd o && make

  install-commands:
  - cd o && make DESTDIR="%{install-root}" install

  strip-commands:
  - |
    %{stage2-strip-binaries}
