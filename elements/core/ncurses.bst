kind: manual
depends:
- gnu-toolchain.bst
sources:
- kind: git
  url: upstream:ncurses
  track: ncurses-5.9-20150131
  ref: f6d73a10a980bc78969c3af93665cbe7d06c3646
config:
  configure-commands:
  - LDCONFIG=true ./configure --with-shared --without-debug --enable-widec
  build-commands:
  - make
  install-commands:
  - make  DESTDIR="%{install-root}" install
  - |
    # lfs recommends some alterations for software that looks for
    # libcurses instead of libncurses
    # it's more convenient to have this in a separate file than the
    # morph as less strings have to be escaped and comments are possible

    # some software expects to find libcurses in /lib
    mkdir -p "%{install-root}/lib"
    mv "%{install-root}"/"%{prefix}"/lib/libncursesw.so.5* "%{install-root}"/lib
    ln -sf ../../lib/libncursesw.so.5 "%{install-root}"/"%{prefix}"/lib/libncursesw.so

    # some linker tricks for libraries that look for non-wide character
    # versions of ncurses
    for lib in ncurses form panel menu; do
        # remove non-wide shared object
        rm -f "%{install-root}/%{prefix}/lib/lib${lib}.so"
        # use a linker script to find the wide character variant
        echo "INPUT(-l${lib}w)" >"%{install-root}/%{prefix}/lib/lib${lib}.so"
        # symlink the non-shared library
        ln -sf "lib${lib}w.a" "%{install-root}/%{prefix}/lib/lib${lib}.a"
    done
    ln -sf libncurses++w.a "%{install-root}/%{prefix}/lib/libncurses++.a"

    # redirect software that looks for libcurses
    rm -f "%{install-root}/%{prefix}/lib/libcursesw.so"
    echo "INPUT(-lncursesw)" >"%{install-root}/%{prefix}/lib/libcursesw.so"
    ln -sf libncurses.so "%{install-root}/%{prefix}/lib/libcurses.so"
    ln -sf libncursesw.a "%{install-root}/%{prefix}/lib/libcursesw.a"
    ln -sf libncurses.a "%{install-root}/%{prefix}/lib/libcurses.a"

    # install documentation
    mkdir -p "%{install-root}/%{prefix}/share/doc/ncurses"
    cp -R doc/* "%{install-root}/%{prefix}/share/doc/ncurses"

    # remove 'clear' and 'reset' executables, busybox provides them
    rm -f "%{install-root}/%{prefix}/bin/clear" "%{install-root}/%{prefix}/bin/reset"
