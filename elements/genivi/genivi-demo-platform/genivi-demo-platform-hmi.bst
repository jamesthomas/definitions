kind: manual
depends:
- genivi/genivi-demo-platform-libs.bst
- qt5-tools.bst
- qt5-tools-qtmultimedia.bst
- qt5-tools-qtwebkit.bst
- genivi/genivi.bst
- wayland-generic.bst
- genivi/weston-genivi.bst
sources:
- kind: git
  url: upstream:genivi/genivi-demo-platform-hmi
  track: master
  ref: fd2c954165b0c0f7f4d9e94e8c9c3c8541a53e79
config:
  configure-commands:
  - |
    sed -i 's/sd_journal_print(LOG_DEBUG,/printf("DEBUG: "/' $(find . -name '*.cpp' -o -name '*.h')
  - |
    sed -i 's/sd_journal_print(LOG_INFO,/printf("INFO: "/' $(find . -name '*.cpp' -o -name '*.h')
  - |
    sed -i 's/sd_journal_print(LOG_ERR,/printf("ERR: "/' $(find . -name '*.cpp' -o -name '*.h')
  - autoreconf -vfi && ./configure --prefix="%{prefix}"
  - cd app/gdp-hmi-background && qmake
  - cd app/gdp-hmi-launcher2 && qmake
  - cd app/gdp-hmi-panel && qmake
  - cd app/qml-example && qmake
  build-commands:
  - make
  - cd app/gdp-hmi-background && make
  - cd app/gdp-hmi-launcher2 && make
  - cd app/gdp-hmi-panel && make
  - cd app/qml-example && make
  install-commands:
  - make DESTDIR="%{install-root}" install
  - cd app/gdp-hmi-background && make INSTALL_ROOT="%{install-root}" install
  - cd app/gdp-hmi-launcher2 && make INSTALL_ROOT="%{install-root}" install
  - cd app/gdp-hmi-panel && make INSTALL_ROOT="%{install-root}" install
  - cd app/qml-example && make INSTALL_ROOT="%{install-root}" install
  - mkdir -p "%{install-root}"/usr/share/gdp
  - cp -a app/gdp-hmi-background/assets/* "%{install-root}"/usr/share/gdp/
  - cp -a app/gdp-hmi-launcher2/content/images/* "%{install-root}"/usr/share/gdp/
  - cp -a app/gdp-hmi-panel/assets/* "%{install-root}"/usr/share/gdp/
  - mkdir -p "%{install-root}/usr/lib/systemd/user"
  - mkdir -p "%{install-root}/usr/lib/systemd/user/default.target.wants"
  - |
    cat >"%{install-root}/usr/lib/systemd/user/gdp-hmi-launcher2.service" <<EOF
    [Unit]
    Description=GENIVI Demo Platform HMI - Launcher2
    Requires=gdp-hmi-controller.service

    [Service]
    Environment=LD_PRELOAD=/usr/lib/libEGL.so
    ExecStart=/usr/bin/gdp-hmi-launcher2

    [Install]
    WantedBy=default.target
    EOF
  - cd "%{install-root}/usr/lib/systemd/user/default.target.wants" && ln -s ../gdp-hmi-launcher2.service
    .
  - |
    cat >"%{install-root}/usr/lib/systemd/user/gdp-hmi-background.service" <<EOF
    [Unit]
    Description=GENIVI Demo Platform HMI - Background
    Requires=gdp-hmi-controller.service
    After=gdp-hmi-controller.service

    [Service]
    Environment=LD_PRELOAD=/usr/lib/libEGL.so
    ExecStart=/usr/bin/gdp-hmi-background

    [Install]
    WantedBy=default.target
    EOF
  - cd "%{install-root}/usr/lib/systemd/user/default.target.wants" && ln -s ../gdp-hmi-background.service
    .
  - |
    cat >"%{install-root}/usr/lib/systemd/user/gdp-hmi-controller.service" <<EOF
    [Unit]
    Description=GENIVI Demo Platform HMI - Controller
    Requires=dbus.service
    After=dbus.service weston.service

    [Service]
    ExecStart=/usr/bin/gdp-hmi-controller
    Restart=always
    RestartSec=2
    TimeoutStopSec=1

    [Install]
    WantedBy=default.target
    EOF
  - cd "%{install-root}/usr/lib/systemd/user/default.target.wants" && ln -s ../gdp-hmi-controller.service
    .
  - |
    cat >"%{install-root}/usr/lib/systemd/user/gdp-hmi-panel.service" <<EOF
    [Unit]
    Description=GENIVI Demo Platform HMI - Panel
    Requires=gdp-hmi-controller.service
    After=gdp-hmi-background.service
    After=EGLWLMockNavigation.service
    After=EGLWLInputEventExample.service
    After=demoui.service
    After=qml-example.service

    [Service]
    Environment=LD_PRELOAD=/usr/lib/libEGL.so
    ExecStart=/usr/bin/gdp-hmi-panel

    [Install]
    WantedBy=default.target
    EOF
  - mkdir -p "%{install-root}/usr/lib/systemd/user/EGLWLMockNavigation.service.wants"
  - cd "%{install-root}/usr/lib/systemd/user/EGLWLMockNavigation.service.wants" &&
    ln -s ../gdp-hmi-panel.service .
  - mkdir -p "%{install-root}/usr/lib/systemd/user/EGLWLInputEventExample.service.wants"
  - cd "%{install-root}/usr/lib/systemd/user/EGLWLInputEventExample.service.wants"
    && ln -s ../gdp-hmi-panel.service .
  - mkdir -p "%{install-root}/usr/lib/systemd/user/demoui.service.wants"
  - cd "%{install-root}/usr/lib/systemd/user/demoui.service.wants" && ln -s ../gdp-hmi-panel.service
    .
  - mkdir -p "%{install-root}/usr/lib/systemd/user/qml-example.service.wants"
  - cd "%{install-root}/usr/lib/systemd/user/qml-example.service.wants" && ln -s ../gdp-hmi-panel.service
    .
  - |
    cat >"%{install-root}/usr/lib/systemd/user/EGLWLMockNavigation.service" <<EOF
    [Unit]
    Description=EGL Wayland Mock Navigation

    [Service]
    ExecStart=/usr/bin/EGLWLMockNavigation -surface 10
    EOF
  - |
    cat > "%{install-root}/usr/lib/systemd/user/EGLWLInputEventExample.service" <<EOF
    [Unit]
    Description=EGL Wayland Input Event Example

    [Service]
    ExecStart=/usr/bin/EGLWLInputEventExample --surface=5100
    EOF
  - |
    cat > "%{install-root}/usr/lib/systemd/user/qml-example.service" <<EOF
    [Unit]
    Description=Genivi QML Example

    [Service]
    ExecStart=/usr/bin/qml-example
    EOF
  - |
    cat > "%{install-root}/usr/lib/systemd/user/PowerOff.service" <<EOF
    [Unit]
    Description=HMI poweroff

    [Service]
    ExecStart=/bin/systemctl poweroff
    EOF
