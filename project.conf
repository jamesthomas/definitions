name: baserock

aliases:
  upstream: git://git.baserock.org/delta/
  baserock: git://git.baserock.org/baserock/
  gnomesdk: https://sdk.gnome.org/

# All the elements go here
element-path: ./elements

# We list a lot of architectures here and Baserock has supported each of these
# at some point in the past, however for the list of *currently* supported
# architectures look in `elements/gnu-toolchain/base.bst`.

options:
  build_arch:
    description: |
      Specifies architecture of the build sandbox.

      This defaults to $(uname -a). On platforms where the Baserock architecture
      name doesn't match the Linux architecture name, you need to manually
      specify the Baserock architecture name.

      You can also use this to build for a different architecture provided your
      host can execute binaries for that architecture. For example, you could
      run an x86_32 build on an x86_64 machine by setting `build_arch=x86_32`
      and `arch=x86_32`.
    type: arch
    variable: build_arch
    values: [
      armv5l, armv7b, armv7l, armv7lhf, armv8l64, armv8b64, mips64b, mips64l,
      mips32b, mips32l, ppc64b, ppc64l, x86_32, x86_64
    ]

  arch:
    description: |
      Specifies host and target architecture for output binaries.

      Usually this should match build_arch. If it differs, a cross compile
      will happen up to the gnu-toolchain/stage2.bst stack. Beyond this,
      cross compilation isn't supported.

      For instructions on how to bootstrap a new architecture, see:
      http://wiki.baserock.org/guides/how-to-cross-bootstrap/
    type: arch
    variable: arch
    values: [
      armv5l, armv7b, armv7l, armv7lhf, armv8l64, armv8b64, mips64b, mips64l,
      mips32b, mips32l, ppc64b, ppc64l, x86_32, x86_64
    ]

variables:
  cpu: "%{arch}"
  abi: gnu

  (?):
  - arch == "x86_32":
      cpu: i686
  - arch in ["armv5l", "armv7b", "armv7l", "armv7lhf"]:
      cpu: "%{arch}"
      abi: gnueabi
  - arch == "armv8l64":
      cpu: aarch64
  - arch == "armv8b64":
      cpu: aarch64_be
  - arch == "mips64b":
      cpu: mips64
      abi: gnuabi64
  - arch == "mips64l":
      cpu: mips64el
  - arch == "mips32b":
      cpu: mips
  - arch == "mips32l":
      cpu: mipsel
  - arch == "ppc64b":
      cpu: powerpc64
  - arch == "ppc64l":
      cpu: powerpc64le

  target-stage1: "%{cpu}-bootstrap-linux-%{abi}"
  target: "%{cpu}-baserock-linux-%{abi}"

  # This should match the %{strip-binaries} variable in BuildStream's default
  # project config. But when doing a cross-build of stage2.bst we need to force
  # use the cross binutils; the native binutils from the base sysroot will
  # ignore the non-native binaries and leave them unstripped.
  stage2-strip-binaries: |
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
      -exec sh -ec \
      'read -n4 hdr <"$1" # check for elf header
       if [ "$hdr" != "$(printf \\x7fELF)" ]; then
           exit 0
       fi
       debugfile="%{install-root}%{debugdir}/$(basename "$1")"
       mkdir -p "$(dirname "$debugfile")"
       %{target-stage1}-objcopy --only-keep-debug "$1" "$debugfile"
       chmod 644 "$debugfile"
       %{target-stage1}-strip --remove-section=.comment --remove-section=.note --strip-unneeded "$1"
       %{target-stage1}-objcopy --add-gnu-debuglink "$debugfile" "$1"' - {} ';'

  # BuildStream passes `--compress-debug-sections` to objcopy; this would be
  # great if it worked but in stage3 builds I see errors like this:
  #
  #    BFD: /buildstream/install/usr/bin/gcc-ranlib: unable to initialize compress status for section .debug_aranges
  #    objcopy:/buildstream/install/usr/bin/gcc-ranlib: Invalid operation
  #
  # This causes debug stripping to just not happen, which makes the stage3
  # sysroot huge. I haven't investigated the cause, but I guess we need to
  # build zlib in stage2 for the feature to work.
  stage3-strip-binaries: |
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
      -exec sh -ec \
      'read -n4 hdr <"$1" # check for elf header
       if [ "$hdr" != "$(printf \\x7fELF)" ]; then
           exit 0
       fi
       debugfile="%{install-root}%{debugdir}/$(basename "$1")"
       mkdir -p "$(dirname "$debugfile")"
       objcopy --only-keep-debug "$1" "$debugfile"
       chmod 644 "$debugfile"
       strip --remove-section=.comment --remove-section=.note --strip-unneeded "$1"
       objcopy --add-gnu-debuglink "$debugfile" "$1"' - {} ';'


artifacts:
  url: https://ostree.baserock.org/cache/

split-rules:
  devel:
    (>):
      - |
        %{prefix}/src/**

plugins:
  - origin: pip
    package-name: buildstream-external
    elements:
      x86image: 0
